const HomePage = require('../pageobjects/homePage');
const HotelListPage = require('../pageobjects/hotelListPage');

describe('booking.com page', () => {
    beforeEach(function () {
        browser.reloadSession();
        HomePage.openHomePage()
    });
    it('should be able to specify age of each child', () => {
        HomePage.guestsSelect();
        HomePage.selectChildrenNumber(3);
        expect(HomePage.enteredChildrenNumber.value).toEqual(HomePage.childAgeInputsNumber.length);
    });
    it('should be able to choose city from the menu', () => {
        HomePage.selectCityFromList();
        HomePage.searchButtonClick();
        expect(HotelListPage.dateInputCalendar).toExist();
        expect(HotelListPage.hotelList).toExist();

    });
    it('should be able to click "show prices" button', () => {
        HomePage.selectCityFromList();
        HomePage.searchButtonClick();
        HotelListPage.cityFieldClick();
        HotelListPage.showPricesButtonClick();
        expect(HotelListPage.dateInputCalendar).toExist();
    });
    it('should set the dates and submit the form', () => {
        HomePage.selectCityFromList();
        HomePage.searchButtonClick();
        HotelListPage.cityFieldClick();
        HotelListPage.showPricesButtonClick();
        HotelListPage.selectTodaysDate();
        HotelListPage.searchButtonClick();
        expect(HotelListPage.checkForZeroPlaces() && HotelListPage.priceFieldsFomHotelEntites.length).toEqual(HotelListPage.allHotelEntities.length);
    })
});