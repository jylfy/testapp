class HomePage {
    openHomePage(){
        browser.url('https://booking.com');
    }

    guestsSelect() {
        $('#xp__guests__toggle').click();
    }

    selectChildrenNumber(number) {
        for (let i = 0; i < number; i++) {
            $('.sb-group-children .bui-stepper__add-button ').click();
        }
    }

    selectCityFromList(){
        $('#ss').click();
        $('.sb-autocomplete__item--city').click();
    }

    searchButtonClick(){
        $('.sb-searchbox__button').click();
    }

    get enteredChildrenNumber () { return $('.sb-group-children .bui-stepper__display ') }

    get childAgeInputsNumber () { return $('.sb-group__children__field > select') }

}

module.exports = new HomePage();
