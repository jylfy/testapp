class hotelListPage {
    cityFieldClick() {
        $('#ss').click();
    }

    showPricesButtonClick() {
        $('#hotellist_inner .bui-button--primary').click();
    }

    selectTodaysDate() {
        $('.bui-calendar__date--today').click();
    }

    searchButtonClick() {
        $('.sb-searchbox__button ').click();
    }

    checkForZeroPlaces(){
        return $('h1').getText().match(/\sзнайдено\s/g);
    }

    get dateInputCalendar() {
        return $('.bui-calendar')
    }

    get hotelList() {
        return $('.hotellist_wrap')
    }

    get allHotelEntities() {
        return $('#hotellist_inner > div[data-hotelid]')
    }

    get priceFieldsFomHotelEntites() {
        return $('#hotellist_inner > div[data-hotelid] .roomPrice')
    }
}

module.exports = new hotelListPage();
